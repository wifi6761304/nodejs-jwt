import express from "express"
import jwt from "jsonwebtoken"
import "dotenv/config"

const app = express()

/**
 * Authenticate user by valid JWT
 * @param Object req
 * @param Object res
 * @param function next
 * @returns
 */
function authenticateToken(req, res, next) {
	const authHeader = req.headers["authorization"]
	// Unauthorized
	if (!authHeader) return res.sendStatus(401)

	const token = authHeader.split(" ")[1]
	// Unauthorized
	if (!token) return res.sendStatus(401)

	// Prüfen, ob Token valide ist, wir erhalten ein Objekt (siehe verifyAccessToken return)
	const result = verifyAccessToken(token)

	if (result.success === false) {
		return res.sendStatus(403)
	}

	// Wir schreiben die entschlüsselten Userdaten in den Request
	// Die nächste Middleware kann nun darauf zugreifen
	req.user = result.data
	next()

}

/**
 * Token generieren, übergebene Daten im Token ablegen
 * Token Provider simuliert
 * @param Object user
 * @returns
 */
function generateAccessToken(user) {
	// Payload Daten werden im Token gespeichert
	const payload = {
		id: user.id,
		email: user.email
	}

	const secret = process.env.TOKEN_SECRET
	// Gültigkeit eine Stunde
	const options = { expiresIn: "1h" }

	return jwt.sign(payload, secret, options)
}

/**
 * Verify token
 * @param string token
 * @returns
 */
function verifyAccessToken(token) {
	// Mit dem dem selben Schlüssel entschlüsseln wir den Token
	const secret = process.env.TOKEN_SECRET

	try {
		// jwt verify function
		const decodedData = jwt.verify(token, secret)
		return {success: true, data: decodedData}
	} catch (error) {
		return {success: false, error: error.message}
	}
}

/**
 * ACHTUNG: eine reine Testroute, um rasch bestimmte Funktionalitäten der App testen zu können.
 * NICHT in produktivem System verwenden!!!!
 */
app.get("/", (req, res) => {
	const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IlRob21hcyBNYWNoZXIiLCJlbWFpbCI6InRvbUB0b20udGVzdCIsImlhdCI6MTcxMDE2OTQ4NywiZXhwIjoxNzEwMTczMDg3fQ.SnJlhNNDvBgdb5D78ZUhPRjVZ0gFn7OMjFcNDSZaWW0"

	res.send(verifyAccessToken(token))
})

/**
 * ACHTUNG: Nur zum Testen gedacht, keinesfalls produktiv veröffentlichen
 */
app.get("/token-helper", (req, res) => {
	const user = {
		id: "Thomas Macher",
		email: "tom@tom.test"
	}

	res.send(generateAccessToken(user))
})

/**
 * Wir setzen im Thunderclient folgende Settings:
 * Unter Auth/Bearer kopieren wir ein gültiges Token in das Token Feld
 *
 * Diese route simuliert einen JSON Web API
 *
 * authentcateToken Middleware überprüft, ob wir überhaupt Zugriff auf die Seite haben
 */
app.get("/protected", authenticateToken, (req, res) => {
	res.json({ message: 'Welcome to the protected admin interface!', user: req.user });
})

app.listen(3000, () => {
	console.log("Server started on http://localhost:3000")
})