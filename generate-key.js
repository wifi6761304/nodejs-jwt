/*
	Example for generating a key for JWT
*/
import crypto from "node:crypto";

/**
 * Create 256 Byte random String and return it in hexadecimal format
 * @param int length
 * @returns
 */
function generateKey(length = 256) {
    return crypto.randomBytes(length).toString('hex');
}

const jwtKey = generateKey();
console.log(jwtKey);